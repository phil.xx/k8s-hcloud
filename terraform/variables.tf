variable "k8s" {
  description = "Map of project names to configuration."
  type        = map
  default = {
    server01 = {
      server_type   = "cx21",
      k8s_node_type = "master"
    },
    node01 = {
      server_type   = "cpx11",
      k8s_node_type = "worker"
    },
    node02 = {
      server_type   = "cpx11",
      k8s_node_type = "worker"
    }
  }
}
