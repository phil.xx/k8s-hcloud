output "ssh_id" {
  description = "The name of the Auto Scaling Group"
  value       = hcloud_ssh_key.users.id
}
