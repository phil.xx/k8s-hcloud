resource "hcloud_ssh_key" "users" {
  name       = "Philip Welz"
  public_key = file("~/.ssh/id_ed25519.pub")
}
