resource "hcloud_server" "server" {
  name        = var.name
  location    = var.location
  image       = var.image
  server_type = var.server_type
  ssh_keys    = var.ssh_keys
  labels = {
    "kubernetes.io/controlplane" = var.label == "master" ? "true" : "false"
    "kubernetes.io/worker"       = var.label == "worker" ? "true" : "false"
  }
  network {
    network_id = var.network_id
  }
  depends_on = [
    var.dependancy
  ]
}


