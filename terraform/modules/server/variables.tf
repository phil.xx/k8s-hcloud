###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "name" {
  description = "(Required, string) Name of the server to create."
  default     = "server"
}

variable "location" {
  description = "(Optional, string) The location name to create the server in. (nbg1, fsn1 or hel1)"
  default     = "fsn1"
}

variable "image" {
  description = "(Required, string) Name or ID of the image the server is created from."
  #default     = "debian-10"
  default     = "ubuntu-20.04"
}

variable "server_type" {
  description = " (Required, string) Name of the server type this server should be created with."
  default     = "cx11" # 1 vCPU, 2 GB RAM, 20 GB NVMe SSD
}

variable "network_id" {
  description = "(int) Unique ID of the network."
  default     = "module.network.network_id"
}

variable "dependancy" {
  description = "(Required, string) Name of the server type this server should be created with."
  default     = "module.network.subnet_id"
}

variable "ssh_keys" {
  description = "(Optional, list) SSH key IDs or names which should be injected into the server at creation time."
  default     = "pub-key"
}

variable "label" {
  description = "(Optional, map) User-defined labels (key-value pairs) should be created with."
  default     = "false"
}
