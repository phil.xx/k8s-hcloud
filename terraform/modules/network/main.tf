resource "hcloud_network" "k8s" {
  name     = var.name
  ip_range = var.ip_range
}

resource "hcloud_network_subnet" "k8s" {
  network_id   = hcloud_network.k8s.id
  type         = var.subnet_type
  network_zone = var.subnet_network_zone
  ip_range     = var.subnet_ip_range
}