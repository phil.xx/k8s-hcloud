output "network_id" {
  description = "The name of the Auto Scaling Group"
  value       = hcloud_network.k8s.id
}

output "subnet_id" {
  description = "The name of the Auto Scaling Group"
  value       = hcloud_network_subnet.k8s.id
}