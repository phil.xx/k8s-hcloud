###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################

variable "name" {
  description = "(Required, string) Name of the Network to create (must be unique per project)"
  default     = "k8s"
}

variable "ip_range" {
  description = "(Required, string) IP Range of the whole Network which must span all included subnets and route destinations."
  default     = "10.30.0.0/16"
}

variable "subnet_network_zone" {
  description = "(Required, string) Name of network zone"
  default     = "eu-central"
}

variable "subnet_ip_range" {
  description = " (Required, string) Range to allocate IPs from. Must be a subnet of the ip_range of the Network."
  default     = "10.30.0.0/24"
}

variable "subnet_type" {
  description = "(Required, string) Type of subnet (server, cloud or vswitch)"
  default     = "cloud"
}
