terraform {
  required_version = "<= 0.14"
  backend "http" {
  }
}

module "ssh" {
  source = "./modules/ssh"
}

module "network" {
  source = "./modules/network"
}

module "server" {
  for_each    = var.k8s
  source      = "./modules/server"
  ssh_keys    = [module.ssh.ssh_id]
  server_type = each.value.server_type
  name        = "k8s-${each.key}"
  network_id  = module.network.network_id
  dependancy  = module.network.subnet_id
  label       = each.value.k8s_node_type
}
