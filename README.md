[[_TOC_]]

# k8s on hcloud

Auto-Provisioning of Controlplane & Worker nodes for a k8s-Cluster on Hetzner-Cloud.

## Inspiration

Heavily inspired by @ekeih sessions at everyonecancontribute.com

- [Repository from @ekeih with k3s](https://gitlab.com/ekeih/k3s-demo)

## Requirements

* Ansible 2.10.x installed
* Terraform 0.13.x installed

* Install Hetzner Cloud (hcloud) module
  * ```bash
    pip3 install hcloud
    ```
* Generate SSH Key
  * ```bash
     ssh-keygen -t ed25519 -C "EMAIL" -f ~/.ssh/hcloud-k8s
    ```
* Set GitLab API Token for the Terraform http backend
  * ```bash
    export GITLAB_TOKEN=TOKEN
    ```
* Set Hetzner Cloud Token
  * ```bash
    export HCLOUD_TOKEN=TOKEN
    ```

## Terraform

### Modules Variables

#### root

* `server` - (Required, string) Map with specifications from where will be created from.

#### network

* `name` - (Required, string) Name of the Network to create (must be unique per project).
* `ip_range` - (Required, string) IP Range of the whole Network which must span all included subnets and route destinations.
* `subnet_network_zone` - (Required, string) Name of network zone.
* `subnet_ip_range` - (Required, string) Range to allocate IPs from. Must be a subnet of the ip_range of the Network.
* `subnet_type` - (Required, string) Type of subnet (server, cloud or vswitch)

### server

* `name` - (Required, string) Name of the server to create.
* `location` - (Optional, string) The location name to create the server in. (nbg1, fsn1 or hel1).
* `image` - (Required, string) Name or ID of the image the server is created from.
* `server_type` -  (Required, string) Name of the server type this server should be created with.
* `network_id` - (int) Unique ID of the network..
* `dependancy` - (optional) If true, do not upgrade the disk. This allows downgrading the server type later, defaults to true.
* `ssh_keys` - (Optional, list) SSH key IDs or names which should be injected into the server at creation time.
* `label` - (Optional, map) User-defined labels (key-value pairs) should be created with.

### Defaults

* Default TF provisioning
  * 1 Control Plane Node (CX21)
  * 2 Worker Nodes (CX11)

### Init (optional)

* Replace `PROJECT_ID` with your GitLab Project ID
* Replace `USER` with your GitLab Username

```bash
# Basic run with local state file
terraform init
#State file in Gitlab
#Replace `PROJECT_ID` with your GitLab Project ID
#Replace `USER` with your GitLab Username
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/PROJECT_ID/terraform/state/default" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/PROJECT_ID/terraform/state/default/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/PROJECT_ID/terraform/state/default/lock" \
    -backend-config="username=USER" \
    -backend-config="password=${GITLAB_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
# create and view an execution plan
terraform plan
# apply the changes
terraform apply
# destroy the Terraform-managed infrastructure
terraform destroy
```

## Ansible
```bash
ansible-playbook hcloud_k8s.yaml --inventory=inventory/hcloud.yml
```

### Cloud Controller Manager

```bash
kubectl -n kube-system create secret generic hcloud --from-literal=token=${HCLOUD_TOKEN} --from-literal=network=k8s
kubectl -n kube-system apply -f hcloud/ccm.yaml
```

### Linting
```bash
yamllint --strict .
ansible-lint ./ansible/hcloud_k8s.yaml
```
