# runs TF init with GitLab HTTP Backend
init:
	#!/usr/bin/env sh
	terraform init \
	-backend-config="address=https://gitlab.com/api/v4/projects/24198520/terraform/state/default" \
	-backend-config="lock_address=https://gitlab.com/api/v4/projects/24198520/terraform/state/default/lock" \
	-backend-config="unlock_address=https://gitlab.com/api/v4/projects/24198520/terraform/state/default/lock" \
	-backend-config="username=phil.xx" \
	-backend-config="password=${GITLAB_TOKEN}" \
	-backend-config="lock_method=POST" \
	-backend-config="unlock_method=DELETE" \
	-backend-config="retry_wait_min=5" \
	./terraform

# runs TF plan
plan:
	#!/usr/bin/env sh
	terraform plan ./terraform

# runs TF apply with auto-approve
apply:
	#!/usr/bin/env sh
	terraform apply -auto-approve ./terraform

# runs the ansible playbook
play:
	#!/usr/bin/env sh
	ansible-playbook ./ansible/hcloud_k8s.yaml --inventory=./ansible/inventory/hcloud.yml

# creates secret for the CCM and applies ccm manifest
ccm:
	#!/usr/bin/env sh
	kubectl -n kube-system create secret generic hcloud --from-literal=token=${HCLOUD_TOKEN} --from-literal=network=k8s
	kubectl apply -f ./hcloud/ccm.yaml

# runs all TF stages, creates the cluster with Ansible and applies the CCM config
create: init plan apply play ccm

# set KUBECONFIG
kubeconfig:
	#!/usr/bin/env sh
	KUBECONFIG=~/.kube/configs/k8s-hcloud

# runs Kubeone reset & TF destroy
destroy:
	#!/usr/bin/env sh
	terraform destroy -auto-approve ./terraform

	rm -f ~/.kube/configs/k8s-hcloud
