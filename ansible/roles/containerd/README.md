[[_TOC_]]

# Ansible Role: Containerd

## Inspiration

Inspired by @geerlingguy:

- [Repository](https://github.com/geerlingguy/ansible-role-containerd/)

## License

MIT
